package hom5;


import java.util.Arrays;

public class Human {

    private Family family;
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet ;
    private DayOfWeek schedule1;
    private String[][] schedule;

    public void setFamily(Family family) {
    }

    enum DayOfWeek {
    Monday,Sunday ,Thursday ,Wednesda ,Tuesday ,Friday, Saturday;
}
 public Human(){

 }
    public Human(String name,String surname,int year){
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name, String surname, int year, int iq, Pet pet, String [][] schedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public void greetPet(){
        System.out.println("hi " + name);
    }
    public void  greetPet(String species,int age,int trickLevel){
        System.out.println("У меня есть" + species +", ему"+ age + "лет, он"+ (trickLevel > 50? "он очень хитрий":"он не очень хитрый "));

    }
    public String returnnStringSchedule(String[][] arary){
        String string="";

        for(int i = 0;i  < arary.length;i++){
            if(!string.equals(""))string+=", ";
            string+= "["+arary[i][0] + ',' + arary[i][1]+"]";
        }
        string = "["+string+"]";
        return string;
    }
    //у класса Human должен выводить сообщение следующего вида:
    //  Human{name='Michael', surname='Karleone', year=1977, iq=90, mother=Jane Karleone, father=Vito Karleone, pet=dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}}
    @Override
    protected void finalize(){
        System.out.println("Object Family deleted");
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
    @Override
    public  String toString (){
        String message = "Human{name = " + name + " surname = " + surname +" year = "+ year + " iq = " + iq + " schedule = " + Arrays.deepToString(schedule) +
                "}" ;
        System.out.println(message);
        return message;
    }


    public static void main(String[] args)  {
        Pet Mura = new Pet(Pet.Species.CAT,"Lina");
        Mura.setAge(10);
        Mura.setTrickLevel((byte) 40);
        Human John = new Human();
        Human Sam = new Human("Sam","Smith",32);
        Sam.surname ="Smith";
        John.name ="John";
        John.surname = "Smith";
        Human Alena = new Human("Alena","Smith",50);
        Family Johnsons = new Family(Alena,John);
        String [] habits = new String[]{"eat","run","sleep"};
        String [][] schedule =new String[][]{{DayOfWeek.Monday.name(),"go to the swimming pool"},{DayOfWeek.Thursday.name(),"go for a walk"}};
        Sam.schedule = schedule;
        Sam.family = Johnsons;
        Johnsons.setPet(Mura);
        Mura.setHabits(habits);
        John.iq = 2000;
        Sam.iq =100;
        Sam.year = 50;
        John.year = 34;
        Johnsons.toString();

    }


}


