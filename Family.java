package hom5;


import java.util.Arrays;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Family(Human mother, Human father){
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
        children = new Human[0];
    }

    public Human getMother() {
        return mother;
    }
    public int countFamily(Human[] children) {
        int count = 2 + this.children.length;
        return count;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }
    public void addChild(Human[] child) {
        children = child;
    }
    public boolean deleteChild(int index){
        if(index < 0 || children == null || index > (children.length -1)){
            return false;
        }
        children[index].setFamily(null);

        int length = children.length;
        Human [] newChildren = new Human[children.length - 1];

        for (int i = 0; i < index; i++) {
            newChildren[i] = children[i];
        }
        for (int i = index; i < children.length - 1; i++) {
            newChildren[i] = children[i + 1];
        }

        children = newChildren;
        if(length > children.length){
            return true;
        } else return false;
    }

    public boolean deleteChild( Human child){

        if(!Arrays.asList(children).contains(child)){
            return  false;
        }
        int index2 = 0 ;
        for(int j = 0;j < children.length;j++){
            if(children[j].equals(child)){

                child.setFamily(null);
                index2 = j;
            }

        }
        if(index2 < 0 || children == null || index2 > (children.length -1)){
            return false;
        }
        int length = children.length;
        Human [] newChildren = new Human[children.length - 1];

        for (int i = 0; i < index2; i++) {
            newChildren[i] = children[i];
        }
        for (int i = index2; i < children.length - 1; i++) {
            newChildren[i] = children[i + 1];
        }
        children = newChildren;
        if(length > children.length){
            return true;
        } else return false;

    }

    public  void addChild(Human child) {
        Human[] newChildren = new Human[children.length + 1];
        for (int i = 0; i < children.length; i++) {
            newChildren[i] = children[i];
        }
        newChildren[newChildren.length -1] = child;
        children = newChildren;

        child.setFamily(this);
    }

    public Human[] getChildren() {
        return children;
    }
    public String returnnStringChildren(Human[] arary){
        String string="";

        for(int i = 0;i  < arary.length;i++){
            if(!string.equals(""))string+=", ";
            string+= "["+arary[i].getName() + ',' + arary[i].getSurname()+ ',' + arary[i].getYear()+"]";
        }
        string = "["+string+"]";


        return string;
    }

    public void setChildren(String[] children) {
        //this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    protected void finalize(){
        System.out.println("Object Family deleted");
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public  String toString(){
        String str = "";
        for(int i = 0;i < children.length;i++){
            str = str + children[i].toString() ;
        }
        String message =  str + this.father.toString() + this.mother.toString() + this.pet.toString()  ;

        return message;
    }

    public static void main(String[] args) throws Throwable {
        Human Oleg = new Human("mother","Golovach",45);
        Human Oly = new Human("father","Golovach",34);
        Family Golovach = new Family(Oleg, Oly);
        Pet Lina = new Pet(Pet.Species.CAT, "Lina", 5, 40);
        String[] habits = new String[]{"run", "sleep"};
        Lina.setHabits(habits);
        Golovach.setPet(Lina);
        Human Olena = new Human("Olena", "Golovach", 13);
        Golovach.addChild(Olena);

        Golovach.deleteChild(0);
        System.out.println("-------------------");

        Golovach.finalize();
    }
}
