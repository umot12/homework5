package hom5;

import org.junit.jupiter.api.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PetTest {
    private Pet module;

    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Test
    public void  petToString(){
        module = new Pet(Pet.Species.CAT,"lina",5, 60);
        String actual = module.toString();
        String expected = "CAT{nickname=lina, age=5, trickLevel=60 , habits=null}";
        assertEquals(expected,actual);
    }
    @Test

    public void testPetEat(){
        PrintStream old=System.out;
        Pet pet=new Pet();
        System.setOut(new PrintStream(output));
        pet.eat();
        assertEquals(output.toString().replaceAll("\n",""),"я кушаю!","Successfully brings text");

        System.setOut(old);
    }
    @Test

    public void testPetFoul(){
        PrintStream old=System.out;
        Pet pet=new Pet();
        System.setOut(new PrintStream(output));
        pet.foul();
        assertEquals(output.toString().replaceAll("\n",""),"Нужно хорошо замести следы...","Successfully brings text");

        System.setOut(old);
    }
    @Test
    public void testPetRespond(){
        PrintStream old=System.out;
        Pet pet = new Pet(Pet.Species.CAT,"lina",5, 60);
        System.setOut(new PrintStream(output));
        pet.respond(pet);
        assertEquals(output.toString().replaceAll("\n",""),"хояин я lina. Я соскучился","Successfully brings text");

        System.setOut(old);
    }
}
