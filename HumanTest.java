package hom5;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HumanTest {
    private Human module;

    @Test
    public void humanToString() {

        module = new Human("name", "surname", 999);
        String actual = module.toString();
        String expected = "Human{name = name surname = surname year = 999 iq = 0 schedule = null}";
        assertEquals(expected, actual);
    }
}
