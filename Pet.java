package hom5;


import java.util.Arrays;

public class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    enum Species {
        DOG,CAT;
    }
    public Pet(Species species,String nickname){
        this.species = species;
        this.nickname = nickname;
    }
    public Pet(Species species, String nickname,int age,int trickLevel,String [] habits){
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    public Pet(Species species, String nickname,int age,int trickLevel){
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;

    }
    public Pet(){}
    public int getAge() {return age;}

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public void eat() {
        System.out.println("я кушаю!");
    }

    public void respond(Pet pet) {
        System.out.println("хояин я " + nickname + ". Я соскучился");
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
    @Override
    protected void finalize(){
        System.out.println("Object Pet deleted");
    }

    @Override
    public   String  toString(){

        String message =  species + "{nickname=" + nickname +", age=" + age +", trickLevel=" + trickLevel +" , habits=" + Arrays.toString(habits) + "}" ;
        System.out.println(message);
        return message;
    }

    public static void main  (String [] Args)  {

        Pet Igor = new Pet();
        Igor.setSpecies(Species.CAT);
        System.out.println(Igor.getSpecies());
        Igor.nickname ="Igor";
        Igor.age = 1;
        Igor.trickLevel = 1;
        Igor.habits  =  new  String [] {"run","sleep","sport"};
        Igor.finalize();

    }

}
